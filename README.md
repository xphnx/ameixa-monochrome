# No longer maintained in this repo. Moved to main Ameixa repository as a build flavor. This README.md will be kept here for documentation.


![CI badge](https://gitlab.com/xphnx/ameixa-monochrome/badges/master/build.svg)

Monochrome port of Ameixa Icon Pack for Trebuchet, KISS Launcher, OpenLauncher, Lawnchair Launcher, Adw, and many more launchers.
It's a material design inspired theme that aims to provide a consistent and minimalistic look to your device.
Code forked from [Icecons icon pack](https://github.com/1C3/ICEcons).
Only apps hosted in [F-Droid](https://f-droid.org) are supported.

## Contributions are welcome

The main code comes from Ameixa. All changes must be done in the Ameixa repo. After changes are complete, run either monochroming script depending on how you decide to install Inkscape.

[(1)](https://gitlab.com/xphnx/ameixa-monochrome/blob/master/monochroming.sh) <\-- Original script. Please use this script first. The second one is just a workaround but is not at all ideal.
- Locally clone the ameixa and ameixa-monochrome repositories.
- Do things.

[(2)](https://gitlab.com/xphnx/ameixa-monochrome/blob/master/monochroming1.sh) <\-- Made by Taco because he (I) couldn't figure out how to use inkscape with the original monochroming script. USE ONLY IF YOU DON'T KNOW HOW TO USE/RUN THE ORIGINAL SCRIPT.
- Locally clone the ameixa and ameixa-monochrome repositories.
- Download and install Inkscape.
- Find folder where Inkscape files are installed.
- Copy-paste Inkscape files (from installation) into a folder called "inkscape" in the locally cloned ameixa-monochrome repository.
- In Git, type "start monochroming1.sh".

## Screenshots

![Screenshot_20160614-224658](/uploads/902ad1c156c54f0d4d3faa7f1534dc70/Screenshot_20160614-224658.png)
![Screenshot_20160614-224809](/uploads/ed15ce26e5d89aacddf921272feca0ce/Screenshot_20160614-224809.png)

## Installation
<a href="https://f-droid.org/packages/org.xphnx.ameixamonochrome">
    <img src="https://f-droid.org/badge/get-it-on.png"
         alt="Get it on F-Droid" height="80">
</a>
